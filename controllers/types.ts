export interface Article {
  created_at: Date,
  title: string,
  url: string,
  author: string,
  points: number,
  story_text: string,
  comment_text: string,
  num_comments: number,
  story_id: number,
  story_title: string,
  story_url: string,
  parent_id: number,
  created_at_i: number,
  _tags: string[],
  objectID: number,
  _highlightResult: any,
}

export interface CustomQuery {
  limit? : number,
  page? : number,
  _tags? : any,
  title? : any,
  author? : any,
  objectID? : number,
}

export interface Request {
  query : CustomQuery,
}