import axios from "axios";

import { Article, CustomQuery, Request } from "./types";
import { ArticleModel } from "../models/articles";


let articles: Article[] = [];
const deletedArticles: Article[] = [];

export const axiosCall = async () => {
  axios.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
  .then((response: { data: { hits: Article[]; }; }) =>  {
      console.log("Making axios call");
      // store articles from api to articles array
      articles = response.data.hits;
      populateDatabase();
  })
  .catch((error) => {
    console.log(error);
  })
}

const populateDatabase = async () => {
  const docCount = await ArticleModel.collection.countDocuments();
  if (docCount == 0) ArticleModel.insertMany(articles);
  else {
    console.log("Database contains info. Comparing contents...");
    for (let i = 0; i < articles.length; i++) {
      const article = articles[i];
      const idToMatch:number = +article.objectID;
      const matchedArticle = await ArticleModel.collection.findOne({ objectID: idToMatch });
      article.objectID = +article.objectID;
      if (!matchedArticle) {
        deletedArticles.forEach(deletedArticle => {
          if (deletedArticle.objectID !== article.objectID) ArticleModel.collection.insertOne(article);
        });
      }
    }
  }
}

export const getAllArticles = async (req: Request, res: any) => {
  try {

    const query:CustomQuery = {};

    const { page, limit } = req.query;
    const options = {
      page: page || 1,
      limit: limit || 5,
    };

    if (req.query._tags) {
      query._tags = { $all: req.query._tags };
    }

    if (req.query.title) {
      query.title = { $regex: req.query.title };
    }

    if (req.query.author) {
      query.author = { $regex: req.query.author };
    }

    const articlesFromDb: any = await ArticleModel.paginate(query, options);
    res.json(articlesFromDb);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error)
  }
}

export const deleteOneArticle = async (req:Request, res:any) => {
  try {
    const { objectID } = req.query;
    if (!objectID) {
      res.json({ message: "objectID was not specified" });
      return;
    }
    const articleFound = await ArticleModel.findById({ _id: objectID })
    if (articleFound) {
      await ArticleModel.deleteOne({ _id: objectID })
      .then(() => {
        deletedArticles.push(articleFound);
        res.status(200).send({ success: true, deleted_article: articleFound })
      })
      .catch((err: any) => res.json({ error: err }));
    }
    else res.json({ error: "Object not found in Database" })
    
  } catch (error) {
    res.json({ error });
  }
}