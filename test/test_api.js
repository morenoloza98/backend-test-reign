const { doesNotMatch } = require("assert");
const assert = require("assert");
const axios = require("axios");

describe('Axios call is done correctly', () => {
  it('should return status 200 from get', (done) => {
    axios.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs")
    .then((data) => {
      assert.ok(data.status == 200);
      done();
    })
    .catch((err) => done(err));
  })
});

describe('Get all documents from db', () => {
  it('should return status 200 and data should be greater than 0', (done => {
    axios.get("http://localhost:3000/")
    .then((data) => {
      assert.ok(data.status == 200);
      // console.log(data)
      assert.ok(data.data.totalDocs > 0);
      done();
    })
    .catch((err) => done(err));
  }))
});

describe('Results should be paginated with a max of 5 items per page by default', () => {
  it('should return a limit value of 5 from data reponse', (done => {
    axios.get("http://localhost:3000/")
    .then((data) => {
      assert.deepEqual(data.data.limit, 5);
      done();
    })
    .catch((err) => done(err));
  }))
})