import mongoose, { PaginateModel } from "mongoose";
import paginate from "mongoose-paginate-v2";
import { Article } from "../controllers/types";
const Schema = mongoose.Schema;

const ArticleSchema = new Schema({
  created_at: { type: Date },
  title: { type: String },
  url: { type: String },
  author: { type: String },
  points: { type: Number },
  story_text: { type: String },
  comment_text: { type: String },
  num_comments: { type: Number },
  story_id: { type: Number },
  story_title: { type: String },
  story_url: { type: String },
  parent_id: { type: Number },
  created_at_i: { type: Number },
  _tags: { type: Array },
  objectID: { type: Number },
  _highlightResult: { type: Object },
})

ArticleSchema.plugin(paginate);

export const ArticleModel = mongoose.model<Article, PaginateModel<Article>>("articles", ArticleSchema);