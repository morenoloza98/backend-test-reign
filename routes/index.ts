import { Router } from "express";

import { axiosCall, getAllArticles, deleteOneArticle } from "../controllers/articles_controller";

const router = Router();

//Make axios call for the first time
axiosCall();

// One hour to miliseconds
const intervalTimeForCall = 1000*60*60;

// Make axios call every hour
setInterval(() => {
  axiosCall();
}, intervalTimeForCall)


router.get("/", getAllArticles);
router.post("/delete", deleteOneArticle);


export = router;