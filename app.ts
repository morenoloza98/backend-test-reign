import cors from "cors";
import express from "express";
import mongoose from "mongoose";

import { appConfig } from "./configs/app_config";
import routes from "./routes/index";

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }))

const mongoDB = `mongodb://127.0.0.1/${appConfig.mongoDbName}`;
mongoose.connect(mongoDB);

const db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error"));

app.get("/", )

app.use("/",routes);

app.listen(appConfig.expressPort, () => console.log(`Server listening on port ${appConfig.expressPort}`));