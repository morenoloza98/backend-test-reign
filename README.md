<!-- # backend-test-reign



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/morenoloza98/backend-test-reign.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/morenoloza98/backend-test-reign/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information. -->

## Name
Junior Backend Developer Challenge - Hacker News API

## Description
This is a server built with Node.js and Express that calls an API every hour and stores such data in a local mongo database. This server also contains an API to interact with the database (using mongoose) in order to get the articles (paginated with a maximum of 5 articles per page) and these can be filtered by tag, author or title. Lastly, the user can also delete articles from the database.

<!-- ## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge. -->

<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation
For this project to work you will need Node.js, the MongoDB shell and some API platform (or your browser), like Postman, installed in your computer.

- For Node.js installation, follow [this](https://nodejs.org/en/download/)
- For MongoDB shell installation, follow [this](https://www.mongodb.com/try/download/shell)
- Or if you want, you can install these by using [Homebrew](https://brew.sh/)
- For installing Postman client follow [this](https://www.postman.com/downloads/)

After downloading these you will have to clone the repository from this page (create a new folder where you would like to contain the repository).
- Head to the top of the page and click the **Clone** button. You can select to clone with SSH or HTTPS. 
- Click the clipboard icon on the far right of the link to copy the link to your clipboard.
- Open a terminal/cmd/Git bash at the location where you want the repository to be clone
- Type the following command
```
git clone git@gitlab.com:morenoloza98/backend-test-reign.git
```
for SSH or
```
git clone https://gitlab.com/morenoloza98/backend-test-reign.git
```
for HTTPS

Next, you will need to install all the dependencies using npm. 

- Head to the root of the repository and install the dependencies
```
cd backend-test-reign
npm install
```

Before starting, change the name of the .env.example file to .env and set the parameters to something different if desired. By default the API port is set to 3000 and the name of the database is set to hacknews.

The following command will compile the typescript files and start the server
```
npm start
```

For the server to be able to store in a mongoDB, you will have to create the database. For that you will have to run the mongoDB shell previously installed.
```
mongo
```
Then create the database
```
use hacknews
```
Where hacknews is the name of the database that is being used in the server. In case you change the name, do not forget to change it in the .env file (MONGODB_NAME).


## Usage
To use the app you can use any API platform, I suggested Postman, or your browser.

When started, the app will make the call to the API and check if the database is empty and store the values. To get all the stored values you can type the following url in Postman/browser's bar:

If in Postman, the dropdown on the left should be set to GET.

```
http://localhost:[API_PORT]/
```

Where API_PORT should be the port you set on the .env file. The returned results will be paginated with a maximum of 5 items per page or you can specify via a query. If in Postman, you can do this by typing in the params section. You can also filter by tags, title and author using this section or the url. The following table shows the parameters you can send through the query in order to filter, paginate or move through pages.

| Key      | Example Value | Description                                 |
| -------- |:-------------:| ------------------------------------------- |
| limit    | 5             | Number of items to show per page            |
| page     | 2             | Number of the page to show                  |
| _tags    | story         | String of the tag you want to filter by     |
| author   | nodesocket    | String of the author's name to filter by    |
| title    | Deno          | String contained in the title to filter by  |

To use this keys in the url instead of Postman's params you can send them after the '/' character adding a '?' followed by the key of the parameter, an '=' and the value. Example:

```
http://localhost:3000/?limit=5
```

In case you want to send multiple parameters you can use an '&' and add the following parameter. Example:

```
http://localhost:3000/?limit=5&_tags=story
```

Using this app, you can also delete records from the database using a POST (set this in the dropdown just as the GET if in Postman) in the '/delete' route and sending the objectID as a parameter. Example:

```
http://localhost:[API_PORT]/delete?objectID=6203fb174342e5c29f7fee6a
```

An easy way to get the objectID is by using the previously mentioned get and copying the "_id" field which is the unique id that mongo gives to each document/record.

[SwaggerDoc](https://app.swaggerhub.com/apis-docs/morenoloza98/reigN-challenge/0.1)