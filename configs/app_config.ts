import dotenv from "dotenv";

dotenv.config()

export const appConfig = {
  expressPort: process.env.API_PORT,
  mongoDbName: process.env.MONGODB_NAME,  
}

